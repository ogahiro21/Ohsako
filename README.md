# Ohsako
誤差逆伝播法で「半端ない大迫」と「半端な大迫」を判別する。

## Over View
C言語でフレームワークを使わずにニューラルネットワークを作成する。
[ohsakoA.train](./ohsakoA.train)と[ohsakoB.train](./ohsakoB.train)の組み合わせを区別するニューラルネットワークを作成する。   
なお、逐次修正法を使用した。

### 学習結果
![image](/error.png)  
**横軸** : 学習回数  
**縦軸** : 誤差  
一気に誤差が小さくなるためパルス波の様なグラフになった。

## Usage
コンパイル
```
$ gcc ohsako.c -lm
```
実行
```
$ ./a.out [The number of data] [file name] [file name]
```
グラフを表示
```
$ gnuplot
> plot "error.dat" w l
```
